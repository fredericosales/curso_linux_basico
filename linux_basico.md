--- 
title: Conhecendo o Linux - curso básico
author: Frederico | Guilherme [GET Engenharia Computacional]

patat:
    slideLevel: 1
    incrementalLists: true
    theme:
        emph: [vividBlue, onVividBlack, italic]
        strong: [bold]
        imageTarget: [onDullWhite, vividRed]
        syntaxHighlighting:
            decVal: [bold, onDullRed]

...

# GET ENGENHARIA COMPUTACIONAL - Universidade Federal de Juiz de Fora - MG

**Conhecendo o Linux - curso básico**

 __Frederico Sales__
 __Guilherme Felix__

Eng. Computacional
     

	O curso de Engenharia Computacional conta, desde novembro de 2010, 
	com um Grupo de Educação Tutorial (GET). Trata-se de um grupo de 
	bolsistas – denominados GETianos – que, sob a tutoria do professor 
	Elson Magalhães Toledo e com a colaboração de professores de diversos 
	departamentos, principalmente do MAC e DCC, desenvolvem atividades integradas 
	de ensino, pesquisa e extensão.

Site:

_http://www.ufjf.br/get_engcomp/_

Link slides:

_https://bit.ly/2O2Ll50_

Link Imagem:

_https://bit.ly/cursoMint_

# CURSO LINUX BÀSICO

- Conhecendo o sistema
	* /
	* pasta /home (~)
	* gerenciador de arquivos
	* flashdrive (pendrive)
	* antivirus (precisa? depende de você)
	* navegador de internet (browser)
	* como compactar e descompactar arquivos
	* instalar aplicativos (ambiente gráfico e terminal)
	* desinstalar aplicataivos (ambiente gráfico e terminal)
	* segurança básica
- comandos básicos
	* ranger
	* vim ou nano ou emacs
	* voce pode usar o gedit, leafpad ou TextEditor
- sudo
	* quem é o root?
	* su -
	* devo usar o root?

# Um pouco de história:

- Em 1994, Richard Stallman iniciou o projeto GNU;

- Em 1991 Linus Torvalds criou o kernel (LINUX) compatível com as aplicações do projeto GNU.

O que é o sistema operacional?

	É a aplicação responsável por controlar o computador, fazendo que o computador execute as ordens do usuário.

	Sem um sistema Operacional o computador vira um monte de componentes eletrônicos sem utilidade.
 
#  O nome

- O LINUX nada mais é que Linus + Unix [minix].

- Desde então o projeto de escola cresceu junto com a comunidade de mantenedores.

# Mas que danado é esse tal de Linux?

- Ele permite que o computador funcione;

- Você não precisará mais de piratear o M$ Windows

- Ele é grátis

# O temido Linux

1. O Linux não é somente a temida tela preta;

2. O Linux dos anos 1990 não é o mesmo hoje em dia...

3. Ele é tão prático e funcional que não precisa da interface gráfica para funcionar.

4. Para usá-lo no dia a dia você não vai precisar recorrer ao terminal.

5. Dependendo da interface que você escolher tudo pode ser feito como no M$ Windows.

6. O terminal não morde.

7. O Ruindows é um sistema feito pela Micro$oft que o empacota em um DVD e te ...

8. O Linux é um ambiente feito por uma comunidade muito grande.

9. Os demais sistemas (resto) são razoáveis, porém são caros.

10. Seja livre.

# Resumo da confusão

- O Linux é um sistema do código aberto feito pela comunidade.

- Exitem licenças que garantem a liberdade do sistema.

- Código fonte é um monte de texto quase sempre sem sentido, capenga e que só funciona sabe deus como.

- Por meio de compiladores essa sopa de letras geram os programas.

- No código aberto você pode fazer parte da bagunça organizada.

- Os programas que rodam nos outros ambientes também têm código fonte, entretanto fechados.

# Sacrilégios

- O Linux puro é somente o kernel (cerne) do sistema.

- Para funcionar ele precisa de outros pacotes, aplicativos, ambientes gráficos e etc.

- Informalmente chamamos o ambiente de Linux.

# Coisas para saber antes de usar o Linux:

1. Linux não é o todo, Linux é somente o Kernel do sistema.

2. Não é preciso ser NERD para usar o pinguim. Mas você precisa no mínimo ser curioso.

3. Você podera ser livre (baixar, modificar, redistribuir conforme sua necessidade).

4. 99% mais seguro que os demais sistemas e 1% é por sua conta é risco.

5. Não existe call center para suporte, porém, existem milhões de pessoas para te ajudar.

6. Com 27 anos de existência ele já é lider no mercado de servidores.

7. Quem subir primeiro puxa o outro. (colaboração mútua da cominidade).

8. Use sem olhar para os lados. (Não se compara bananas com maças...).

9. Todos fazem parte do mesmo time. (A escolha de uma distribuição é livre).

10. Talvez ele não atenda as suas necessidades.

# Que distribuição devo usar?

- Essa é uma questão filosófica.

- Existem distros fáceis de usar... 

- Existem distros que são voltadas para servidores...

- Existem distros que são bonitas...

- Existem distros para você.

- Nesse curso vamos falar do Linux Mint

# Linux Mint

- ___Por quê?___

# Linux Mint

- __Essa é fácil__, porque essa é uma distribuição de entrada.

- Mas o que é distribuição de  entrada?

- link do site:
	* ___https://linuxmint.com/___

- link para baixar:
	* ___https://linuxmint.com/edition.php?id=256 (64-bits) <<___
	* ___https://linuxmint.com/edition.php?id=253 (32-bits)___

- link para a documentação:
	* ___https://linuxmint-installation-guide.readthedocs.io/pt_BR/latest/___
	* ___https://linuxmint.com/documentation/user-guide/Cinnamon/portuguese_brazil_18.0.pdf___

- link para o etcher
	* ___https://etcher.io/___

# Linux Mint

- O objetivo do Mint é produzir um sistema moderno e confortável para novos usuários.

- É uma das distribuições mais populares.

- Funciona out of the box (bate pronto).

- É livre e grátis.

- Usuários são encorajados a enviar feedback sobre possíveis problemas e/ou novas ideias.

- Baseado no Debian - Mas usa os repositórios do Ubuntu.

- É seguro e confiável.

# O que não fazer (10 mandamentos)

1. Jamais deverá usar o root.

2. Não uses o DD em vão.

3. Não abuse do SUDO.

4. Nunca usará RM -RF .

5. Não conversará com o vim.

6. Jamais, jamais usarei o FORTUNE e XCOWSAY.

7. MAHJONG é pior que SUDOKU e LOL. 

8. Se você instalar o WINE, vai arder no marmore do inferno. 

9. Nunca edit o FSTAB

10. Nunca brincarás com o grub.

# As aplicações

	Muitos dos programas que usamos no dia a dia já estão instalados por padrão no Mint.

	Navegadores de internet:

	- Firefox
	- Google Chrome (chromium)
	- Opera

# As aplicações

	Clientes de E-mail:
	- Evolution
	- Mozilla Thunderbird
 
# As aplicações

	Pacotes Office:
	- Libreoffice
	- Office360 (online)

# As aplicações

	Editores de imagens:
	- GIMP (substituto livre do ADOBE PhotoShop)

# As aplicações

    Editores Vetoriais
	- InkScape (substituto do CorelDraw)

# As aplicações

    Visualizadores de Documentos:
	- Evince
	- Adobe Acrobat Reader

# As aplicações

    CAD:
	- Openshape (online)
	- Fusion360 (online)
	- DraftSight
	- BRL-CAD
	- FREECAD
	- LibreCad
	- OpenScad
	- QCAD

# Gerenciador de Arquivos

- terminal
	* ls
	* cd
	* cp
	* mv
	* mkdir
	* rm
	* ln
	* vim, nano, emacs
	* grep
	* top, htop
	* ip
	* touch
- ranger
	* modo terminal
- Gerenciador de arquivos
	* modo janela
 
# Browsers (navegadores de internet)

- lynx
	* modo terminal
- Google Chrome / Google Chromium
- Mozilla Firefox
- Opera
- Vivaldi

# Clientes de E-mail

- WebMail
- Evolution
- Thunderbird

# Pacotes Office

- LibreOffice
- LibreOffice (web)
- GoogleDocs (web)
- M$ Office360 (web)
- WPS -> Clone livre do M$ Office
- Python + pandas + numpy [pode substituir aquela ferramenta famosa da M$ E]
- vim [pode substituir aquela ferramenta famosa da M$ W]

# Editores de Imagens

- Gimp
- Pixeluvo
- Pencilsheep

# Editores vetoriais

- InkScape
- Pinta
- Krita
- Gimp

# Editores de video

- Shotcut
- Kdenlive
- Pitvi

# OBRIGADO

	OBRIGADO 
	PELA
	ATENÇÃO

# ALGUMA DÚVIDA

```python
#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
GET Engenharia Computacional
"""

# import
import socket

def main(localrtr='172.16.0.254'):
    """
    Some doc

    :param: localrtr: local router
    :return: addr: Local IP address
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((localrtr, 80))
    addr = s.getsockname()
    s.clode()

    return addr[0]

if __name__ == '__main__:
    print(f'Local IP address: {main()}')

```

