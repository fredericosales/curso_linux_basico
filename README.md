# Curso Linux Básico

## Para abrir o slide
Você vai precisar usar patat. Para instalar
abra um terminal e digite o seguinte comando:

$ sudo apt install patat 

## link para o repositório gitlab
https://bit.ly/2O2Ll50

## link para a imagem
https://bit.ly/cursoMint

## Get Engenharia Computacional

![](img/get_logo.png)


## Semana da Engenharia

![](img/ufjf.jpeg)

## Linus Torvalds

![](img/linus.jpg)


## Tux

![](img/tux.png)